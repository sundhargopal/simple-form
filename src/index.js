import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import ja from 'react-intl/locale-data/ja';

// Our translated strings
import localeData from './translatios/translatios.json';

addLocaleData([...en, ...ja]);

// Define user's language. Different browsers have the user locale defined
// on different fields on the `navigator` object, so we make sure to account
// for these different by checking all of them

const language =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage;

//const language = 'ja';

// Split locales with a region code
const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];

// Try full locale, fallback to locale without region code, fallback to en
const messages =
  localeData[language] ||
  localeData[languageWithoutRegionCode] ||
  localeData.en;

//const messages = localeData[language];

// Render our root component into the div with id "root"

// If browser doesn't support Intl (i.e. Safari), then we manually import
// the intl polyfill and locale data.
if (!window.Intl) {
  (async () => {
    try {
      await import('react-intl');
      await import('react-intl/locale-data/en.js');
      await import('react-intl/locale-data/ja.js');
      ReactDOM.render(
        <IntlProvider locale={language} messages={messages}>
          <App />
        </IntlProvider>,
        document.getElementById('root')
      );
    } catch (error) {
      console.log(error);
    }
  })();
} else {
  ReactDOM.render(
    <IntlProvider locale={language} messages={messages}>
      <App />
    </IntlProvider>,
    document.getElementById('root')
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
